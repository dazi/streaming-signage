#! /bin/bash

# TV einschalten über HDMI-Power-Befehl
echo "on 0" | sudo cec-client -s -d 1

# Laeuft "omxplayer.bin" noch? Wenn ja, beende den alten Prozess
if [ -n "$(pidof omxplayer.bin)" ]
then
kill $(pidof omxplayer.bin) 2> /dev/null
fi

# Laeuft "fbi" (PID vorhanden) noch? Wenn ja, beende den alten Prozess
if [ -n "$(pidof fbi)" ]
then
kill $(pidof fbi) 2> /dev/null
fi

#omxplayer http://192.168.90.180:8000/main   &
#omxplayer rtmp://62.113.210.250/medienasa-live/elbe_high  2&>1 /dev/null
# get rid of the cursor so we don't see it when videos are running
setterm -cursor off
 
# set here the path to the directory containing your videos
VIDEOPATH="/media/bilder/" 
 
# you can normally leave this alone
SERVICE="omxplayer"
 
# check every second of omxplayer still runs, ele restart with all items in VIDEOPATH
while true; do
	if ps ax | grep -v grep | grep $SERVICE > /dev/null
	then
        sleep 1;
	else
        for entry in $VIDEOPATH/*
        do
			clear
			omxplayer --no-osd -b -o hdmi $entry > /dev/null
        done
fi
done
