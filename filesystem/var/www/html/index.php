<?php if (isset($_POST['praesentation-aus'])) { exec('sudo ./logo.sh'); } ?>
<?php if (isset($_POST['praesentation-an'])) { exec('sudo ./praesentation.sh'); } ?>
<?php if (isset($_POST['logo-an'])) { exec('sudo ./logo.sh'); } ?>
<?php if (isset($_POST['video-an'])) { exec('sudo ./video.sh &'); } ?>
<?php if (isset($_POST['video-aus'])) { exec('sudo ./logo.sh'); } ?>
<?php if (isset($_POST['tv-an'])) { exec('echo "on 0" | sudo cec-client -s -d 1'); } ?>
<?php if (isset($_POST['tv-aus'])) { exec('echo "standby 0" | sudo cec-client -s -d 1  2> /dev/null'); } ?>

<html>
<head>
<title>Steuerung</title>
<meta charset="utf-8"/>
<meta http-equiv="expires" content="0"/>
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes"/>
<meta name="description" content="Digital Signage mit einem Raspberry Pi, einem lighttpd-Webserver"/>
<meta name="keywords" content="Raspberry Pi, Fernseher"/>
<style>
body {
background-color: white;
font-family: Monospace;
font-size: 14px;
font-color: #3b5998;
}
h2 {
color: #3b5998;
}
a {
color: black;
text-decoration: underline;
}
a:active,a:visited {
color: black;
}
a:hover {
text-decoration: none;
color: black;
}
p {
color: red;
}

button.anButton {
   font-size: 13px;
   color: #000000;
   font-weight: normal;
   font-style: normal;
   background-color: #E4E4E4;
   width: 50px;
   cursor: pointer;
   border: 1px solid #576675;
}

button.anButton:hover {
   font-size: 13px;
   color: #FFFFFF;
   font-weight: normal;
   font-style: normal;
   background-color: #59983b;
   width: 50px;
   cursor: pointer;
   border: 1px solid #FFFFFF;
}

button.ausButton {
   font-size: 13px;
   color: #000000;
   font-weight: normal;
   font-style: normal;
   background-color: #E4E4E4;
   width: 60px;
   cursor: pointer;
   border: 1px solid #576675;
}

button.ausButton:hover {
   font-size: 13px;
   color: #FFFFFF;
   font-weight: normal;
   font-style: normal;
   background-color: #FF0000;
   width: 60px;
   cursor: pointer;
   border: 1px solid #983b59;
}

footer {
font-size: 12px;
}
</style>
</head>
<body>
<main>
<h2>Steuerung Kleinkind</h2>
<h3>Flachbildschirm</h3>
<form action="" method="post">
<button name="tv-an" type="submit" class="anButton">An</button>
<button name="tv-aus" type="submit" class="ausButton">Aus</button>
</form>
<h3>TL-Logo</h3>
<form action="" method="post">
<button name="logo-an" type="submit" class="anButton">An</button>
<!-- <button name="bild-aus" type="submit" class="ausButton">Aus</button> -->
</form>
<h3>Präsentation</h3>
<form action="" method="post">
<button name="praesentation-an" type="submit" class="anButton">An</button>
<button name="praesentation-aus" type="submit" class="ausButton">Aus</button>
</form>
<h3>Video</h3>
<form action="" method="post">
<button name="video-an" type="submit" class="anButton">An</button>
<button name="video-aus" type="submit" class="ausButton">Aus</button>
</form>
</main>
</body>
</html>
