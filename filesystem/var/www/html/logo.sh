#! /bin/bash
# TV einschalten, falls dieser in standby
echo "on 0" | sudo cec-client -s -d 1
# Laeuft "omxplayer.bin" (PID vorhanden)? Wenn ja, beende den Prozess
if [ -n "$(pidof omxplayer.bin)" ]
then
kill $(pidof omxplayer.bin) 2> /dev/null
fi

# Laeuft "fbi" (PID vorhanden)? Wenn ja, beende den alten Prozess später am Ende
ps -aux | grep fbi | grep -v grep
PID=0
if [ -n "$(pidof fbi)" ]
then
PID=$(pidof fbi)
echo "vorgaenger: "$PID
#kill $(pidof fbi) 2> /dev/null
fi

# Starte das Logo 
fbi -T 1 -noverbose -once /usr/share/pixel-wallpaper/logo.png  2> /dev/null

# Alter Prozess von "fbi" wird beendet
if [ $PID != 0 ]
then
kill $PID 2> /dev/null
fi
ps -aux | grep fbi | grep -v grep
