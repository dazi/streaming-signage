#! /bin/bash

# TV einschalten, falls dieser in standby
echo "on 0" | sudo cec-client -s -d 1

# Laeuft "omxplayer.bin" (PID vorhanden)? Wenn ja, beende den Prozess
if [ -n "$(pidof omxplayer.bin)" ]
then
kill $(pidof omxplayer.bin) 2> /dev/null
fi

# Laeuft "fbi" (PID vorhanden)? Wenn ja, beende den Prozess
if [ -n "$(pidof fbi)" ]
then
kill $(pidof fbi) 2> /dev/null
fi

# Starte die Praesentation 
fbi -T 1 -noverbose  -d /dev/fb0 -t 5  /media/bilder/*  2> /dev/null
