#! /bin/bash

# TV einschalten, falls dieser in standby
echo "on 0" | sudo cec-client -s -d 1

# Laeuft "omxplayer.bin" (PID vorhanden)? Wenn ja, beende den Prozess
if [ -n "$(pidof omxplayer.bin)" ]
then
kill $(pidof omxplayer.bin) 2> /dev/null
fi

# Laeuft "fbi" (PID vorhanden)? Wenn ja, beende den Prozess
if [ -n "$(pidof fbi)" ]
then
kill $(pidof fbi) 2> /dev/null
fi

omxplayer http://192.168.90.180:8000/main   &

#omxplayer http://192.168.23.36:8000/main   &
#omxplayer rtmp://62.113.210.250/medienasa-live/elbe_high  2> /dev/null
#(omxplayer rtmp://62.113.210.250/medienasa-live/elbe_high &);

