# streaming-signage

Easy versatile system for distributing live audio/video and digital signage over Ethernet

## AVB

A real low latency solution can only be built using AVB. This is planned for a later version of this project.

## Extender

The easiest solution is to use a Non-IEEE 802 Ethernet standard conform HDMI extender like  DIGITUS Professional DS-55120. It costs about 100 EUR per set and can be extended with many receivers, so one HDMI source is streamed with low latency to many sinks.

A consumer grade camcorder like Panasonic 777 cannot use a Line level signal into its microphone input, and the built-in microphone is no usable for live production.
So an audio inserter has to be used like **deleyCON 4K HDMI Audio Inserter**. This one can use digital audio via SPDIF or Toslink or analog mini TRS 3,5mm. It costs about 35EUR.

In order to record the video signal in parallel, after this audio inserter a HDMI switch can be used like **CSL - 4k UHD HDMI Splitter CEC - HDCP**. This second output can now be connected to a frame grabber connected to a PC or to an ( ethernet rtp/IP) encoder or a recorder.

## RasPi

Since it's dead cheap, does HDMI out and a webserver and media playback, it's very convenient to have an RTP stream via ethernet, or other media, played in the TV.

Next to the video stream every type of media can be played back, even automatically and in an endless loop.

Since it's a real linux distribution, a media folder is mounted and every user in the Network can put files into it, to be played as soon the slideshow is restarted.

The audio can be routed via HDMI out as well, and either the built in bad speakers of the screen or it's external output can be used for audio playback.

### Install

1. boot raspi
    sudo apt-get install samba lightdpd fbi cec-client php7.0 php7.0-common php7.0-cgi
    sudo light-enable-mod fastcgi-php
    sudo chmod g+s /var/www
    sudo chown www-data:www-data /var/www
    sudo vim /etc/samba/smb.conf
    sudo visudo www-data ADMIN NOPASSWD

2. change picture in .share/ logo
3. wallpaper /usr/share/rpd-wallpaper.pnd
4. screensaver off
5. start silent, no console output during boot time
6. mouse off: /etc/lightdm/lightdm.conf xserver-command = X -nocursor
 or lxe unclutter -idle 5
use multiple receiver raspis: ./frame.php iframes von anderen in einem webserver

The ip adresse is optained via dhcp.

The ssh password of the image can be obtained from the author of this file.

To create a slideshow you can use:
    fbi -a -t 5 *.jpg

Please see for the options to scale and preprocess images.

Videos are not possible with fbi.