#! /bin/bash
# create backup
sudo dd bs=4M if=/dev/mmcblk0 | gzip > /home/christian/RaspberryPi/tvtest`date +%d%m%y`.gz
# recreate backup image
# gzip -dc /home/christian/RaspberryPi/tvtest121016.gz | sudo dd bs=4M of=/dev/mmcblk0
# check status of dd in second shell
sudo pkill -USR1 -x dd
# Abfrage SD-Karte
sudo fdisk -l
